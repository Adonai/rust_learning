// Input: A branch root pointing to the root of a well-formed tree and an
// integer max_height giving the height of the tree.
// Output: The x and y fields of each node are set so that the tree is
// positioned with as narrow a width as possible. We assume that the
// height of each node has been correctly set.
// Method: A counter holding the next free x-coordinate is kept for each
// level of the tree. We assume that each node has a width and height of
// one unit and that there should be one unit gaps between the levels of
// the tree and between the nodes across a level. In this and later
// algorithms, spacing between levels or nodes can be changed by
// modifying the spacing constants. This algorithm positions parents
// before children; any tree walk is acceptable so long as each node is
// visited after its relatives to the left on the same level. All
// programs assume that the father of the root is nil.

input root : branch;
max_height : integer;
var next_x: array [O..max_height] of integer;
    current : branch;
    i : integer;
begin
    (* for each level assign 1 to available X *)
    (* by level I mean height, but I see it as if the tree had different floors or levels *)
    (* basically initialize the available X's array *)
    for i := 0 to max_height do next_x[i] := 1; end for;

    (* by looking at the code, seems like status represents *)
    (* the number of this node's children already processed *)
    (* if status = 1 it means only this node's been processed *)
    (* but none of its children *)
    (* in the end it should be equal to # of children *)
    (* status is then used to traverse this node's children *)
    (* it acts as an index of the current children being processed *)
    root.status := 0;

    current := root;
    while current != nil do
        if current.status = 0 then
            (* get available X in this level *)
            (* for root this is 1 *)
            current.x := next_x[current.height];

            current.y := 2 * current.height + 1;

            (* current level (current.height) is 0 for root *)
            (* set current level's X to X+2 *)
            (* for root this is 1 + 2 = 3 *)
            (* the 2 might be due to the current node ocupying 1 space *)
            (* plus a space that separates this node from the next one *)
            next_x[current.height] := next_x[current.height] + 2;

            (* set this node's children as unprocessed *)
            for i := 1 to current.#_of_sons do
                current.son[i].status := 0; end for;

            (* finished this node's processing *)
            current.status := 1;

        elseif 1 <= current.status
                & current.status <= current.#_of_sons then
            current.status := current.status + 1;
            current := current.son[current.status-1];
        else (* current.status > current.#_of_sons *)
            current := current.father;
        fi;
    end while;
end; (* of Algorithm 1 *)
