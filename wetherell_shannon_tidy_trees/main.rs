use std::collections::VecDeque;
use std::convert::TryInto;
use std::option::Option;

#[derive(Debug)]
struct Node {
    value: String,
    left: Option<Box<Node>>,
    right: Option<Box<Node>>,
    height: i32,
    x: i32,
    y: i32,
}

impl Node {
    fn default() -> Self {
        Node {
            value: String::from(""),
            left: None,
            right: None,
            height: 0,
            x: 0,
            y: 0,
        }
    }

    fn set_height(&mut self, prev_height: i32) {
        self.height = prev_height + 1;

        if let Some(left) = &mut self.left {
            left.set_height(self.height);
        }
        if let Some(right) = &mut self.right {
            right.set_height(self.height);
        }
    }

    fn print(&self) {
        println!("Node{{value: {}, height: {}, x: {}, y: {}}}", self.value, self.height, self.x, self.y);

        if let Some(left) = &self.left {
            left.print();
        }
        if let Some(right) = &self.right {
            right.print();
        }
    }

    fn get_max_height(&self) -> i32 {
        let mut queue: Vec<&Node> = vec![self];
        let mut i = 0;

        let mut max_height = 0;
        while i < queue.len() {
            let current = queue[i];

            max_height = max_height.max(current.height);

            if let Some(left) = &current.left {
                queue.push(left);
            }
            if let Some(right) = &current.right {
                queue.push(right);
            }

            i += 1;
        }
        return max_height;
    }
}

fn main() {
    let mut root: Node = gen_tree();

    root.set_height(-1);

    let max_height = root.get_max_height();
    set_node_positions(&mut root, max_height);

    root.print();

    println!("max_height: {}", root.get_max_height());
}

fn set_node_positions(node: &mut Node, max_height: i32) {
    let mut nodes: VecDeque<&mut Node> = VecDeque::new();

    nodes.push_back(node);

    let levels: usize = (max_height + 1).try_into().unwrap();
    let mut next_x: Vec<i32> = vec![1; levels];

    while let Some(current) = nodes.pop_front() {
        current.x = next_x[current.height as usize];
        current.y = (current.height + 1) * 2;

        next_x[current.height as usize] = current.x + 2;

        if let Some(left) = &mut current.left {
            nodes.push_back(left);
        }
        if let Some(right) = &mut current.right {
            nodes.push_back(right);
        }
    }
}

fn gen_tree() -> Node {
    let node4 = Node {
        value: String::from("4"),
        ..Node::default()
    };
    let node5 = Node {
        value: String::from("5"),
        ..Node::default()
    };
    let node2 = Node {
        value: String::from("2"),
        left: Some(Box::new(node4)),
        right: Some(Box::new(node5)),
        ..Node::default()
    };

    let node6 = Node {
        value: String::from("6"),
        ..Node::default()
    };
    let node7 = Node {
        value: String::from("7"),
        ..Node::default()
    };
    let node3 = Node {
        value: String::from("3"),
        left: Some(Box::new(node6)),
        right: Some(Box::new(node7)),
        ..Node::default()
    };

    let root = Node {
        value: String::from("1"),
        left: Some(Box::new(node2)),
        right: Some(Box::new(node3)),
        ..Node::default()
    };
    return root;
}
