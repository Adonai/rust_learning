use rand::Rng;
use std::{cmp::Ordering, io};

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    println!("The secret number is: {secret_number}");

    loop {
        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        // variable shadowing it's when we redefine a variable, using
        // the same name. The compiler allows this and the benefit
        // is that we can change the value and even the type of the
        // original variable without declaring more variable names.
        // The difference between a mut variable and a shadowed one is
        // that a mut variable will always be mutable, while a
        // shadowed one will only be mutated when we redefine it, but
        // afterwards it will not.
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!(r#"You guessed: {guess}"#);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too large!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
